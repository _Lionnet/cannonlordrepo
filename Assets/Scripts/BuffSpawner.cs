﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffSpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject[] buffs, Spawns;
    private GameObject currentPoint;
    int indexSpawn;
    int indexBuff;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnBuff());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator SpawnBuff()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(7,20));
            indexSpawn = Random.Range(0, Spawns.Length);
            indexBuff = Random.Range(0, buffs.Length);
            currentPoint = Spawns[indexSpawn];
            Instantiate(buffs[indexBuff], currentPoint.transform.position, Quaternion.identity);
        }   


    }

}
