﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{
    public GameObject game, canvas, restart;
    public Image vidaPlayer;
    public static bool lost = false;

    public void StartGame()
    {
        game.SetActive(true);
        canvas.SetActive(false);
    }

    public void Restart()
    {
        lost = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        
    }

    private void Update()
    {
        if(vidaPlayer.fillAmount < 0.2f)
        {
            Time.timeScale = 1;
            lost = true;
            restart.SetActive(true);
        }
    }
}
