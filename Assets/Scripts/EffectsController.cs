﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class EffectsController : MonoBehaviour
{
    PostProcessVolume volume;
    Vignette vignetteLayer = null;
    Bloom bloomLayer = null;

    float vigIntensity = 0.45f;
    public float bloom = 5f;
    public float saturation = 5f;

    // Start is called before the first frame update
    void Start()
    {
        volume = this.gameObject.GetComponent<PostProcessVolume>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Buffs.PowerShooting == true)
        {
            volume.profile.TryGetSettings(out vignetteLayer);
            volume.profile.TryGetSettings(out bloomLayer);

            vignetteLayer.enabled.value = true;
            vignetteLayer.intensity.value = vigIntensity;

            bloomLayer.enabled.value = true;
            bloomLayer.intensity.value = 0.00003f;
            bloomLayer.color.value.r = 252;
            bloomLayer.color.value.g = 0;
            bloomLayer.color.value.b = 255;

            StartCoroutine(BackToNormal());
        }
    }

    IEnumerator BackToNormal()
    {
        yield return new WaitForSeconds(4f);
        vignetteLayer.intensity.value = 0;

        bloomLayer.intensity.value = 0;
        bloomLayer.color.value.g = 0;
        bloomLayer.color.value.b = 255;
        bloomLayer.color.value.r = 0;
        bloomLayer.color.value.g = 0;
        bloomLayer.color.value.b = 0;
    }
}
