﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buffs : MonoBehaviour
{
    [SerializeField]
    private GameObject[] PowerShots;
    GameObject laser;
    public static bool PowerShooting = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Tiro" && this.gameObject.name == "L(Clone)")
        {
            PowerShooting = true;
            Color tmp = this.gameObject.GetComponent<SpriteRenderer>().color;
            tmp.a = 0f;
            print("qweqwe");
            laser = Instantiate(PowerShots[0], GameObject.Find("canhao").gameObject.transform, false);
            StartCoroutine(EndPower());
            this.gameObject.GetComponent<SpriteRenderer>().color = tmp;
        }
    }

    IEnumerator EndPower()
    {
        yield return new WaitForSeconds(4);
        Destroy(laser);
        Destroy(this.gameObject);
        PowerShooting = false;
    }
}
