﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bats : MonoBehaviour
{
    [SerializeField]
    private float speed = 5;

    [SerializeField]
    private float MinX, MaxX;
    private int Direction;
    private float fEnemyX;

    public static int Pontos;

    public static Text PontosLabel;

    public static Image playerVida;

    private void Awake()
    {
        PontosLabel = GameObject.Find("PointsNumber").GetComponent<Text>();
        playerVida = GameObject.Find("VidaBar").GetComponent<Image>();
    }

    // Start is called before the first frame update
    void Start()
    {
        Destroy(this.gameObject, 7);
    }

    // Update is called once per frame
    void Update()
    {
        Movement();   
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Tiro")
        {
            Pontos += 10;
            PontosLabel.text = Pontos.ToString();
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
            
        }
        if(collision.tag == "TiroForte")
        {
            Pontos += 10;
            PontosLabel.text = Pontos.ToString();
            Destroy(this.gameObject);
        }
        if(collision.tag == "Base")
        {
            playerVida.fillAmount -= 0.2f;
        }
    }

    private void Movement()
    {
        
        float fEnemyX = this.gameObject.transform.localPosition.x;
        //float step = speed * Time.deltaTime;
        //transform.position = Vector2.MoveTowards(transform.position, GameObject.Find("Player").transform.position, step);

        switch (Direction)
        {
            case 0:
                // Moving Left
                if (fEnemyX > MinX)
                {
                    this.gameObject.transform.position -= new Vector3(2.0f, 2, 0) * Time.deltaTime;
                }
                else
                {
                    // Hit left boundary, change direction
                    Direction = 1;
                }
                break;

            case 1:
                // Moving Right
                if (fEnemyX < MaxX)
                {
                    this.gameObject.transform.position += new Vector3(2.0f, -2, 0) * Time.deltaTime;
                }
                else
                {
                    // Hit right boundary, change direction
                    Direction = 0;
                }
                break;
        }

    }
}
