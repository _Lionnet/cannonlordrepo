﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    private GameObject tiro, firePoint, firePointDad;

    private GameObject[] tiros;
    private float[] teste;
    private float[] teste2;
    private float actualRot;
    int controller = -1;
    // Start is called before the first frame update
    void Start()
    {
        //teste = new float[100];
        Manager.lost = false;
        tiros = new GameObject[9000];
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && GameObject.FindGameObjectsWithTag("Tiro").Length == 0 && Buffs.PowerShooting == false && Manager.lost == false)
        {
            this.gameObject.GetComponent<AudioSource>().Play();
            controller++;
            actualRot = -this.transform.GetChild(0).localRotation.z / 10;
            //teste[controller] = firePointDad.transform.localRotation.z;
            GameObject fire = (GameObject)Instantiate(tiro, firePoint.transform.position, Quaternion.identity);
            //Go(fire);
            tiros[controller] = fire;
            Destroy(fire, 1.2f);
            //fire.transform.Translate(new Vector2(this.transform.position.x, 1));            

        }

        if(controller >= 0)
        {
            if(tiros[controller] != null)
            Go(tiros[controller], actualRot);
        }


    }

    private void Go(GameObject shoot, float Xangle)
    {
        shoot.transform.Translate(new Vector2(Xangle, 0.05f));
    }
}
