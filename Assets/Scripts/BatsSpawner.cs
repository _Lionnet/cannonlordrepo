﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatsSpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject[] Spawns;
    public GameObject Bat;
    private GameObject currentPoint;
    int index;
    int controller = 1;
    float initialTime = 1;
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnBat());
    }

    // Update is called once per frame
    void Update()
    {
        //StartCoroutine(SpawnBat());
    }

    IEnumerator SpawnBat()
    {
        while (true)
        {
            if (controller % 17 == 0 && initialTime > 0.61)
            {
                initialTime = initialTime / 1.07f;
            }
                

            yield return new WaitForSeconds(initialTime);
            index = Random.Range(0, Spawns.Length);
            currentPoint = Spawns[index];
            Instantiate(Bat, currentPoint.transform.position, Quaternion.identity);
            controller++;
        }
        
        
    }
}
