Como Baixar:
	Clicar em Downloads no menu lateral (esquerda) e depois selecionar Download Repository.

Como Executar:
	Basta abrir a build do jogo na resolu��o desejada e jogar.
	Comandos: Clique do mouse para intera��o com bot�es; Barra de Espa�o para atirar.

Organiza��o do C�digo:
	O c�digo foi organizado mantendo os scripts referentes a cada objeto separados, parar tornar o ambiente mais organizado. Vari�veis privadas foram amplamente utilizadas, principalnente pela necessidade de instanciar m�ltiplos objetos com caracter�sticas iguais, mas com valores independentes (como os tiros e morcegos). O projeto teve cada tipo de asset divido pelo seu tipo de conte�do, para facilitar a localiza��o dos mesmos.

Pontos Adicionais:
	Achei que seria interessante adicionar sons e alguns sprites diferentes ao projeto, logo, ap�s concluir o jogo b�sico, me dediquei a buscar assets visuais e sonoros para enaltecer o projeto, e com esse mesmo intuito, tamb�m apliquei efeitos de p�s-processamento.

Cr�ditos:
	M�sica: Bensound (Royalty Free Music) - Badass
	Raio Laser: Marvel vs Capcom 2 Sprite
	Bala de canh�o: BIONICLE: The Legend of Mata Nui